package com.gm.gsmc.pix.model.vehicle;

import com.gm.gsmc.pix.model.settings.SettingDescriptor;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class VehicleTest {
    private Vehicle vehicle1;

    private VehicleCapability capability1;
    private VehicleCapability capability2;
    private VehicleCapability capability3;

    private MakeModelYear mmy1;

    @Before
    public void setUp() {

        mmy1 = new MakeModelYear(2016, "GMC", "Sierra");
//        vehicle1 = new Vehicle("VIN123", mmy1, CountryIsoCode.US);

        capability1 = new VehicleCapability("Heated Seat Setting", 0, 5);
        capability2 = new VehicleCapability("Radio Level", 0, 10);
        capability3 = new VehicleCapability();
        capability3.setName("Sirius XM Enabled");

    }

    @Test
    public void getNullCapabilities(){
        Vehicle vehicle = new Vehicle();
        List<VehicleCapability> capabilities = new ArrayList<>();
        vehicle.setCapabilities(capabilities);
        assertEquals(vehicle.getCapabilities(), new ArrayList<>());
    }
    @Test
    public void getNullDefaultSettings(){
        Vehicle vehicle = new Vehicle();
        List<SettingDescriptor> defaultSettings = new ArrayList<>();
        vehicle.setDefaultSettings(defaultSettings);
        assertEquals(new ArrayList<>(), vehicle.getDefaultSettings());
    }
    @Test
    public void getNullIsoCode(){
        Vehicle vehicle = new Vehicle();
        assertNull(vehicle.getIsoCode());
        //should result in an error, throw exception for null value
    }
//    @Test
//    public void getNullClientVersion(){
//        Vehicle vehicle = new Vehicle();
//        assertNull(vehicle.getClientVersion());
//    }
//    @Test
//    public void getNullMMY(){
//        Vehicle vehicle = new Vehicle();
//        assertEquals(vehicle.getMakeModelYear(), null);
//    }
//    @Test
//    public void getNullMake(){
//        Vehicle vehicle = new Vehicle();
//        assertEquals(vehicle.getMakeModelYear().getMake(), null);
//    }
//    @Test
//    public void getNullModel(){
//        Vehicle vehicle = new Vehicle();
//        assertEquals(vehicle.getMakeModelYear().getModel(), null);
//    }
    @Test
    public void getYearMakeModel() {
    }

    @Test
    public void getVin() {
    }

    @Test
    public void setVin() {
    }

    @Test
    public void getYear() {
    }

    @Test
    public void setYear() {
    }

    @Test
    public void getMake() {
    }

    @Test
    public void setMake() {
    }

    @Test
    public void getModel() {
    }

    @Test
    public void setModel() {
    }

    @Test
    public void getCapabilities() {
    }

    @Test
    public void setCapabilities() {
    }

    @Test
    public void isInfotainmentDemoState() {
    }

    @Test
    public void setInfotainmentDemoState() {
    }

    @Test
    public void getClientVersion() {
    }

    @Test
    public void setClientVersion() {
    }

    @Test
    public void getDefaultSettings() {
    }

    @Test
    public void setDefaultSettings() {
    }

    @Test
    public void getCountryIsoCode() {
    }

    @Test
    public void setCountryIsoCode() {
    }
}

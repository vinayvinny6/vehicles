package com.gm.gsmc.pix.vehicles.jpa;


import com.gm.gsmc.pix.vehicles.jpa.model.VehiclesEntity;
import org.springframework.data.repository.CrudRepository;

public interface VehiclesRepository extends CrudRepository<VehiclesEntity, String>{

    VehiclesEntity findOneByVin(String vin);

}

package com.gm.gsmc.pix.vehicles.translator.reverse;

import com.gm.gsmc.pix.general.dao.AbstractModelTranslator;
import com.gm.gsmc.pix.model.vehicle.VehicleCapability;
import com.gm.gsmc.pix.vehicles.jpa.model.CapabilityEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReverseCapabilityTranslator extends AbstractModelTranslator<VehicleCapability, CapabilityEntity> {
    @Override
    public CapabilityEntity translate(VehicleCapability original) {
        CapabilityEntity capability = new CapabilityEntity();
        capability.setName(original.getName());
        capability.setMinValue(original.getMinValue());
        capability.setMaxValue(original.getMaxValue());
        return capability;
    }
}

package com.gm.gsmc.pix.vehicles.jpa.model;

import com.gm.gsmc.pix.model.settings.SettingCategory;
import com.gm.gsmc.pix.model.settings.SettingType;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
@Data
@NoArgsConstructor

@Entity
@Table(name = "VEHICLE_SETTINGS")
public class DefaultSettingsEntity implements Serializable{
    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Enumerated (EnumType.STRING)
    private SettingType type;

    private String value;
    @Enumerated (EnumType.STRING)
    private SettingCategory category;

    public DefaultSettingsEntity(SettingCategory category, String name, String value, SettingType type) {
        this.category = category;
        this.name = name;
        this.value = value;
        this.type = type;
    }

    @Override
    public String toString(){
        return "ID:" + id + "    NAME:" + name + "    VALUE:" + value;
    }
}

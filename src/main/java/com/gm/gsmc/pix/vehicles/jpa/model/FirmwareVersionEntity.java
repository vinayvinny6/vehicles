package com.gm.gsmc.pix.vehicles.jpa.model;

import com.gm.gsmc.pix.model.vehicle.InfotainmentInterface;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
@Data
@NoArgsConstructor

@Entity
@Table (name = "VEHICLE_FIRMWARE_VERSION")
public class FirmwareVersionEntity {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;
    private int majorVersion;
    private int minorVersion;
    private int patchVersion;

    @Enumerated (EnumType.STRING)
    private InfotainmentInterface osType;

    public FirmwareVersionEntity (InfotainmentInterface osType, int majorVersion, int minorVersion, int patchVersion) {

        this.osType = osType;
        this.majorVersion = majorVersion;
        this.minorVersion = minorVersion;
        this.patchVersion = patchVersion;

    }
}

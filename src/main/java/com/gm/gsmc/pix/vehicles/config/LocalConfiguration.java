package com.gm.gsmc.pix.vehicles.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile ("default")
public class LocalConfiguration {

    private static final Logger log = LoggerFactory.getLogger(LocalConfiguration.class);

}

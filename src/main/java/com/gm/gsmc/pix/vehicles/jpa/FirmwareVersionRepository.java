package com.gm.gsmc.pix.vehicles.jpa;

import com.gm.gsmc.pix.vehicles.jpa.model.FirmwareVersionEntity;
import org.springframework.data.repository.CrudRepository;

public interface FirmwareVersionRepository extends CrudRepository<FirmwareVersionEntity, Long> {
}

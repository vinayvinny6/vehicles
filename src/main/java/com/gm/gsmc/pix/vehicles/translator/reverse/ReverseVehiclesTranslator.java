package com.gm.gsmc.pix.vehicles.translator.reverse;

import com.gm.gsmc.pix.api.service.PixException;
import com.gm.gsmc.pix.general.dao.AbstractModelTranslator;
import com.gm.gsmc.pix.model.vehicle.Vehicle;
import com.gm.gsmc.pix.vehicles.jpa.model.VehiclesEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class ReverseVehiclesTranslator extends AbstractModelTranslator<Vehicle, VehiclesEntity> {

    private final ReverseCapabilityTranslator reverseCapabilityTranslator;
    private final ReverseDefaultSettingsTranslator reverseDefaultSettingsTranslator;

    @Autowired
    public ReverseVehiclesTranslator(ReverseCapabilityTranslator reverseCapabilityTranslator,
                                     ReverseDefaultSettingsTranslator reverseDefaultSettingsTranslator) {
        this.reverseCapabilityTranslator = reverseCapabilityTranslator;
        this.reverseDefaultSettingsTranslator = reverseDefaultSettingsTranslator;
    }

    @Override
    public VehiclesEntity translate(Vehicle original) {

        if (original == null) {
            throw new PixException(HttpStatus.BAD_REQUEST, "Vehicle is null or does not exist. Translation failed", "VHCL-400");
        }
        VehiclesEntity vehicleEntity = new VehiclesEntity();
        vehicleEntity.setVin(original.getVin());
        vehicleEntity.setMake(original.getMake());
        vehicleEntity.setModel(original.getModel());
        vehicleEntity.setYear(original.getYear());
        vehicleEntity.setRadioType(original.getRadioType());
        vehicleEntity.setRadioLevel(original.getRadioLevel());
        vehicleEntity.setTrim(original.getTrim());
        vehicleEntity.setColor(original.getColor());
        vehicleEntity.setMinorVersion(original.getMinorVersion());
        vehicleEntity.setMajorVersion(original.getMajorVersion());
        vehicleEntity.setPatchVersion(original.getPatchVersion());
        if (original.getIsoCode() != null) {
            vehicleEntity.setIsoCode(original.getIsoCode());
        }
        if (original.getCapabilities() != null)
            vehicleEntity.setCapabilityEntity(reverseCapabilityTranslator.translate(original.getCapabilities()));
        if (original.getDefaultSettings() != null)
            vehicleEntity.setDefaultSettings(reverseDefaultSettingsTranslator.translate(original.getDefaultSettings()));
        if (original.getVehicleState() != null) {
            vehicleEntity.setVehicleState(original.getVehicleState());
        }
        if (original.getVehicleUseType() != null) {
            vehicleEntity.setVehicleUseType(original.getVehicleUseType());
        }
        if (original.getOsType() != null) {
            vehicleEntity.setOsType(original.getOsType());
        }


        return vehicleEntity;
    }
}

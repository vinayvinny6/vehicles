package com.gm.gsmc.pix.vehicles.manager;

import com.gm.gsmc.pix.api.service.PixException;
import com.gm.gsmc.pix.model.CountryIsoCode;
import com.gm.gsmc.pix.model.vehicle.MakeModelYear;
import com.gm.gsmc.pix.model.vehicle.Vehicle;
import com.gm.gsmc.pix.model.vehicle.VehicleCapability;
import com.gm.gsmc.pix.model.vehicle.VehicleState;
import com.gm.gsmc.pix.vehicles.client.UtilityClient;
import com.gm.gsmc.pix.vehicles.exception.VehicleException;
import com.gm.gsmc.pix.vehicles.exception.VehicleExceptionCode;
import com.gm.gsmc.pix.vehicles.jpa.VehiclesRepository;
import com.gm.gsmc.pix.vehicles.jpa.model.VehiclesEntity;
import com.gm.gsmc.pix.vehicles.translator.reverse.ReverseVehiclesTranslator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VehicleUtility {

    private final UtilityClient utilityClient;

    @Autowired
    public VehicleUtility(UtilityClient utilityClient) {
        this.utilityClient = utilityClient;
    }

    public VehiclesEntity getVehicle(VehiclesRepository vehiclesRepo, String vin){
        if (vin == null) {
            throw VehicleExceptionCode.INVALID_VIN.exception();
        }
        VehiclesEntity vehiclesEntity = vehiclesRepo.findOneByVin(vin);
        if (vehiclesEntity == null) {
            throw VehicleExceptionCode.VEHICLE_NOT_FOUND.exception();
        }
        return vehiclesEntity;
    }
    public VehiclesEntity getVehicleCreate(ReverseVehiclesTranslator reverseVehiclesTranslator, Vehicle vehicle){
        if (vehicle == null) {
            throw VehicleExceptionCode.INVALID_VIN.exception();
        }

        VehiclesEntity vehiclesEntity = reverseVehiclesTranslator.translate(vehicle);

        if(vehiclesEntity == null){
            throw new VehicleException(VehicleExceptionCode.VEHICLE_NOT_FOUND);
        }

        MakeModelYear mmy = this.utilityClient.getMMY(vehiclesEntity.getYear(), vehiclesEntity.getMake(), vehiclesEntity.getModel());
        if(mmy == null){
            throw new PixException(HttpStatus.CONFLICT,"MMY of vehicle does not exist in the MMY Master List!","UTIL-???");
        }

        return vehiclesEntity;
    }

    public VehiclesEntity getVehicleCountry(VehiclesRepository vehiclesRepo, String vin, CountryIsoCode isoCode){
        if (vin == null) {
            throw VehicleExceptionCode.INVALID_VIN.exception();
        }
        if(isoCode == null){
            throw VehicleExceptionCode.INVALID_ISO_CODE.exception();
        }
        VehiclesEntity vehiclesEntity = vehiclesRepo.findOneByVin(vin);
        if(vehiclesEntity == null){
            throw VehicleExceptionCode.VEHICLE_NOT_FOUND.exception();
        }
        vehiclesEntity.setIsoCode(isoCode);
        return vehiclesEntity;
    }

    public VehiclesEntity getVehicleCapabilities(VehiclesRepository vehiclesRepo, String vin, List<VehicleCapability> vehicleCapabilities){
        if (vin == null) {
            throw VehicleExceptionCode.INVALID_VIN.exception();
        }
        if(vehicleCapabilities.isEmpty()){
            throw VehicleExceptionCode.INVALID_CAPABILITIES.exception();
        }
        VehiclesEntity vehiclesEntity = vehiclesRepo.findOneByVin(vin);
        if(vehiclesEntity == null){
            throw VehicleExceptionCode.VEHICLE_NOT_FOUND.exception();
        }
        return vehiclesEntity;
    }

    public VehiclesEntity getVehicleState(VehiclesRepository vehiclesRepo, String vin, VehicleState vehicleState){
        if (vin == null) {
            throw VehicleExceptionCode.INVALID_VIN.exception();
        }
        if(vehicleState == null){
            throw VehicleExceptionCode.INVALID_VEHICLE_STATE.exception();
        }
        VehiclesEntity vehiclesEntity = vehiclesRepo.findOneByVin(vin);
        if (vehiclesEntity == null) {
            throw VehicleExceptionCode.VEHICLE_NOT_FOUND.exception();
        }
        vehiclesEntity.setVehicleState(vehicleState);
        return vehiclesEntity;
    }
}

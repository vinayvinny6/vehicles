package com.gm.gsmc.pix.vehicles.jpa.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

/**
 * @author YZ70WZ
 */
@Data
@NoArgsConstructor

@Entity
@Table (name = "VEHICLE_MMY")
public class MMYEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String make;
    private String model;
    private int year;

    public MMYEntity(String make, String model, int year) {

        this.make = make;
        this.model = model;
        this.year = year;

    }
}

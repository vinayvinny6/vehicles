package com.gm.gsmc.pix.vehicles.exception;

public enum VehicleExceptionCode {

    INVALID_VIN("001", "VIN cannot be null or is invalid"),
    INVALID_ISO_CODE("002", "Iso Code is not contained with enum class or is null"),
    INVALID_CAPABILITIES("003", "Vehicle with specified vehicle capabilities are null/invalid"),
    INVALID_VEHICLE_STATE("004", "Vehicle state is not contained with enum class or is null"),
    INVALID_VEHICLE_SETTINGS("005", "Vehicle settings is either an empty list or the list is invalid"),
    INVALID_FIRMWARE_VERSION("006", "Vehicle settings is either an empty list or the list is invalid"),
    INVALID_MMY("007", "MMY object has an invalid structure or is null"),

    VEHICLE_NOT_FOUND("404", "Vehicle object not found!"),
    VEHICLE_CAPABILITIES_NOT_FOUND("404", "Vehicle capabilities not found!"),
    VEHICLE_SETTINGS_NOT_FOUND("404", "Default vehicle settings not found!"),
    INVALID_INPUT("422", "Invalid Input - Cannot be processed!"),
    NO_RESPONSE("444", "No response sent back");

    private static final String prefix = "VCLS";
    private final String code;
    private final String description;

    VehicleExceptionCode(String code, String description) {
        this.code = prefix + "_" + code;
        this.description = description;
    }

    public String getCode() {
        return this.code;
    }

    public String getDescription() {
        return this.description;
    }

    public VehicleException exception(){
        throw new VehicleException(this);
    }
}

package com.gm.gsmc.pix.vehicles.translator.reverse;

import com.gm.gsmc.pix.general.dao.AbstractModelTranslator;
import com.gm.gsmc.pix.model.vehicle.MakeModelYear;
import com.gm.gsmc.pix.vehicles.jpa.model.MMYEntity;
import org.springframework.stereotype.Service;

/**
 * @author YZ70WZ
 */

@Service
public class ReverseMMYTranslator extends AbstractModelTranslator<MakeModelYear, MMYEntity> {
    @Override
    public MMYEntity translate(MakeModelYear original){

        MMYEntity mmyEntity = new MMYEntity();
        mmyEntity.setMake(original.getMake());
        mmyEntity.setModel(original.getModel());
        mmyEntity.setYear(original.getYear());

        return mmyEntity;
    }
}

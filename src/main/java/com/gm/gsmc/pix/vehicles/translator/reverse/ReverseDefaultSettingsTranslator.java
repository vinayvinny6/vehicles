package com.gm.gsmc.pix.vehicles.translator.reverse;


import com.gm.gsmc.pix.general.dao.AbstractModelTranslator;
import com.gm.gsmc.pix.model.settings.SettingDescriptor;
import com.gm.gsmc.pix.vehicles.jpa.model.DefaultSettingsEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReverseDefaultSettingsTranslator extends AbstractModelTranslator<SettingDescriptor, DefaultSettingsEntity> {
    @Override
    public DefaultSettingsEntity translate(SettingDescriptor original){

        DefaultSettingsEntity defaultSettingsEntity = new DefaultSettingsEntity();
        defaultSettingsEntity.setCategory(original.getCategory());
        defaultSettingsEntity.setName(original.getName());
        defaultSettingsEntity.setValue(original.getSelectedValue());
        defaultSettingsEntity.setType(original.getType());

        return defaultSettingsEntity;
    }

}

package com.gm.gsmc.pix.vehicles.manager;

import com.gm.gsmc.pix.model.CountryIsoCode;
import com.gm.gsmc.pix.model.settings.SettingDescriptor;
import com.gm.gsmc.pix.model.vehicle.Vehicle;
import com.gm.gsmc.pix.model.vehicle.VehicleCapability;
import com.gm.gsmc.pix.model.vehicle.VehicleState;
import com.gm.gsmc.pix.vehicles.jpa.CapabilityRepository;
import com.gm.gsmc.pix.vehicles.jpa.VehiclesRepository;
import com.gm.gsmc.pix.vehicles.jpa.model.CapabilityEntity;
import com.gm.gsmc.pix.vehicles.jpa.model.VehiclesEntity;
import com.gm.gsmc.pix.vehicles.translator.VehiclesTranslator;
import com.gm.gsmc.pix.vehicles.translator.reverse.ReverseVehiclesTranslator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class VehiclesManager {

    private final VehiclesRepository vehiclesRepo;
    private final VehiclesTranslator vehiclesTranslator;
    private final ReverseVehiclesTranslator reverseVehiclesTranslator;
    private final CapabilityRepository capabilityRepo;
    private final VehicleUtility vehicleUtility;

    @Autowired
    public VehiclesManager (VehiclesRepository vehiclesRepo, VehiclesTranslator vehiclesTranslator,
            ReverseVehiclesTranslator reverseVehiclesTranslator, CapabilityRepository capabilityRepo, VehicleUtility vehicleUtility) {
        this.vehiclesRepo = vehiclesRepo;
        this.vehiclesTranslator = vehiclesTranslator;
        this.reverseVehiclesTranslator = reverseVehiclesTranslator;
        this.capabilityRepo = capabilityRepo;
        this.vehicleUtility = vehicleUtility;
    }

    public Vehicle createVehicle(Vehicle vehicle) {

        VehiclesEntity vehiclesEntity = this.vehicleUtility.getVehicleCreate(reverseVehiclesTranslator, vehicle);
        this.vehiclesRepo.save(vehiclesEntity);
        return this.vehiclesTranslator.translate(vehiclesEntity);
    }

    public Vehicle getVehicle(String vin) {
        // get Vehicle by VIN
        VehiclesEntity vehiclesEntity = this.vehicleUtility.getVehicle(vehiclesRepo, vin);
        return this.vehiclesTranslator.translate(vehiclesEntity);
    }

    @Cacheable ("vehicleCountry")
    public CountryIsoCode getVehicleCountry(String vin) {
        VehiclesEntity vehiclesEntity = this.vehicleUtility.getVehicle(vehiclesRepo, vin);
        Vehicle vehicle = this.vehiclesTranslator.translate(vehiclesEntity);
        return vehicle.getIsoCode();
    }

    @CachePut (value = "vehicleCountry", key = "#vin")
    public CountryIsoCode updateVehicleCountry(String vin, CountryIsoCode isoCode) {
        VehiclesEntity vehiclesEntity = this.vehicleUtility.getVehicleCountry(vehiclesRepo, vin, isoCode);
        this.vehiclesRepo.save(vehiclesEntity);
        // get actual country and return from DB logic
        Vehicle vehicle = this.vehiclesTranslator.translate(vehiclesEntity);
        return vehicle.getIsoCode();
    }

    public List<VehicleCapability> getVehicleCapabilities(String vin) {
        VehiclesEntity vehiclesEntity = this.vehicleUtility.getVehicle(vehiclesRepo, vin);
        Vehicle vehicle = this.vehiclesTranslator.translate(vehiclesEntity);
        return vehicle.getCapabilities();
    }

    public List<SettingDescriptor> getDefaultVehicleSettings(String vin) {
        VehiclesEntity vehiclesEntity = this.vehicleUtility.getVehicle(vehiclesRepo, vin);
        Vehicle vehicle = this.vehiclesTranslator.translate(vehiclesEntity);
        return vehicle.getDefaultSettings();
    }

    public List<VehicleCapability> setVehicleCapabilities(String vin, List<VehicleCapability> vehicleCapabilities) {
        VehiclesEntity vehiclesEntity = this.vehicleUtility.getVehicleCapabilities(vehiclesRepo, vin, vehicleCapabilities);
        replaceVehicleCapabilities(vehiclesEntity, vehicleCapabilities);
        return vehicleCapabilities;
    }

    public Vehicle updateInfotainmentState(String vin, VehicleState vehicleState) {
        VehiclesEntity vehiclesEntity = this.vehicleUtility.getVehicleState(vehiclesRepo, vin, vehicleState);
        this.vehiclesRepo.save(vehiclesEntity);
        return this.vehiclesTranslator.translate(vehiclesEntity);
    }

    @Transactional
    List<CapabilityEntity> replaceVehicleCapabilities(VehiclesEntity vehiclesEntity, List<VehicleCapability> vehicleCapabilities) {
        // remove old capabilities
        List<CapabilityEntity> oldCapabilities = vehiclesEntity.getCapabilityEntity();
        oldCapabilities.clear();
        this.vehiclesRepo.save(vehiclesEntity);
        // create list of new settings
        List<CapabilityEntity> newCapabilities = new ArrayList<>();

        for (VehicleCapability capability : vehicleCapabilities)
            newCapabilities.add(new CapabilityEntity(capability.getName(), capability.getMinValue(), capability.getMaxValue()));

        this.capabilityRepo.save(newCapabilities);
        return newCapabilities;
    }

}

package com.gm.gsmc.pix.vehicles.client.fallback;

import com.gm.gsmc.pix.model.settings.SettingCategory;
import com.gm.gsmc.pix.model.settings.SettingDescriptor;
import com.gm.gsmc.pix.model.settings.SettingType;
import com.gm.gsmc.pix.model.vehicle.MakeModelYear;
import com.gm.gsmc.pix.vehicles.client.UtilityClient;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UtilityClientFallback implements UtilityClient{

    @Override
    public List<SettingDescriptor> getAllDefaultSettings() {
        return null;
    }

    @Override
    public SettingDescriptor getDefaultSetting(String name) {
        return new SettingDescriptor(SettingCategory.VehicleSetting, "AutoWindowLock", SettingType.BOOLEAN, "1");
    }

    @Override
    public List<SettingDescriptor> getDefaultSettingList(String[] names) {
        List<SettingDescriptor> settingsList = new ArrayList<>();
        //List of settings
        SettingDescriptor setting1 = new SettingDescriptor(SettingCategory.VehicleSetting, "AutoWindowLock", SettingType.BOOLEAN, "1");
        SettingDescriptor setting2 = new SettingDescriptor(SettingCategory.VehicleSetting, "HeatedSeatSetting", SettingType.INT, "5");
        SettingDescriptor setting3 = new SettingDescriptor(SettingCategory.VehicleSetting, "AutoUnlock", SettingType.BOOLEAN, "1");
        //Add settings to List
        settingsList.add(setting1); settingsList.add(setting2); settingsList.add(setting3);

        return settingsList;
    }

    @Override
    public List<MakeModelYear> getAllMMYs() {
        return null;
    }

    @Override
    public MakeModelYear getMMY(int year, String make, String model) {
        return new MakeModelYear(2017, "Chevrolet", "Silverado");
    }

    @Override
    public List<MakeModelYear> getMMYList(List<Long> ids) {
        List<MakeModelYear> mmyList = new ArrayList<>();

        MakeModelYear mmy1 = new MakeModelYear(2017, "Chevrolet", "Silverado");
        MakeModelYear mmy2 = new MakeModelYear(2016, "GMC", "Sierra");
        MakeModelYear mmy3 = new MakeModelYear(2018, "Chevrolet", "Volt");
        mmyList.add(mmy1); mmyList.add(mmy2); mmyList.add(mmy3);

        return mmyList;
    }
}

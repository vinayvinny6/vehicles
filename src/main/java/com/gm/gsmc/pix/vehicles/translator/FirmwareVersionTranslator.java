package com.gm.gsmc.pix.vehicles.translator;

import com.gm.gsmc.pix.general.dao.AbstractModelTranslator;
import com.gm.gsmc.pix.model.vehicle.FirmwareVersion;
import com.gm.gsmc.pix.vehicles.exception.VehicleExceptionCode;
import com.gm.gsmc.pix.vehicles.jpa.model.FirmwareVersionEntity;
import org.springframework.stereotype.Service;

@Service
public class FirmwareVersionTranslator extends AbstractModelTranslator<FirmwareVersionEntity, FirmwareVersion> {
    @Override
    public FirmwareVersion translate(FirmwareVersionEntity original) {
        if (original == null){
            throw VehicleExceptionCode.INVALID_FIRMWARE_VERSION.exception();
        }
        FirmwareVersion vehicleVersion = new FirmwareVersion();
        vehicleVersion.setMajorVersion(original.getMajorVersion());
        vehicleVersion.setMinorVersion(original.getMinorVersion());
        vehicleVersion.setPatchVersion(original.getPatchVersion());
        vehicleVersion.setOsType(original.getOsType());

        return vehicleVersion;
    }

}


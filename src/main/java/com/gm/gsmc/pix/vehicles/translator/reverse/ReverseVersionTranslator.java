package com.gm.gsmc.pix.vehicles.translator.reverse;

import com.gm.gsmc.pix.general.dao.AbstractModelTranslator;
import com.gm.gsmc.pix.model.vehicle.FirmwareVersion;
import com.gm.gsmc.pix.vehicles.jpa.model.FirmwareVersionEntity;
import org.springframework.stereotype.Service;

@Service
public class ReverseVersionTranslator extends AbstractModelTranslator<FirmwareVersion, FirmwareVersionEntity> {
    @Override
    public FirmwareVersionEntity translate(FirmwareVersion original) {
        FirmwareVersionEntity firmwareVersionEntity = new FirmwareVersionEntity();
        firmwareVersionEntity.setMajorVersion(original.getMajorVersion());
        firmwareVersionEntity.setMinorVersion(original.getMinorVersion());
        firmwareVersionEntity.setPatchVersion(original.getPatchVersion());
        firmwareVersionEntity.setOsType(original.getOsType());

        return firmwareVersionEntity;
    }

}




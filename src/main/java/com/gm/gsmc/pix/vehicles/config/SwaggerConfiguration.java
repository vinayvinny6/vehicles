package com.gm.gsmc.pix.vehicles.config;

import org.springframework.context.annotation.Configuration;

import com.gm.gsmc.pix.general.config.AbstractSwaggerBasicConfiguration;

/**
 * @author TZVZCY
 */
@Configuration
public class SwaggerConfiguration extends AbstractSwaggerBasicConfiguration {

    @Override
    protected String getName() {
        return "Vehicles";
    }

    @Override
    protected String getDescription() {
        return "Vehicles Service";
    }

    @Override
    protected String getControllerBasePackage() {
        return "com.gm.gsmc.pix.vehicles.controller";
    }

}

package com.gm.gsmc.pix.vehicles.jpa;

import com.gm.gsmc.pix.vehicles.jpa.model.DefaultSettingsEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

/**
 * @author YZ70WZ
 */

@Service
public interface VehicleSettingsRepository extends CrudRepository<DefaultSettingsEntity, Long> {

}

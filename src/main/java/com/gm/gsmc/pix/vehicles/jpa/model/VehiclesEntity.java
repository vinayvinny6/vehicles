package com.gm.gsmc.pix.vehicles.jpa.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gm.gsmc.pix.model.CountryIsoCode;
import com.gm.gsmc.pix.model.Region;
import com.gm.gsmc.pix.model.vehicle.InfotainmentInterface;
import com.gm.gsmc.pix.model.vehicle.VehicleState;
import com.gm.gsmc.pix.model.vehicle.VehicleUseType;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author YZ70WZ
 */
@Data

@Entity
@Table (name = "VEHICLE")
public class VehiclesEntity implements Serializable {

    @Id
    @GeneratedValue (strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated (EnumType.STRING)
    @JsonIgnore
    private CountryIsoCode isoCode;

    @OneToMany (cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "vehicleId")
    private List<CapabilityEntity> capabilityEntity;

    /*REPLACE THIS WITH GLOBAL SETTINGS ENTITY*/
    @OneToMany (cascade = CascadeType.ALL)
    @JoinColumn (name = "vehicleId")
    private List<DefaultSettingsEntity> defaultSettings;

    private String vin;

    private String make;
    private String model;
    private int year;
    //TODO: Look into moving radio related fields to a MMY object (unsure right now, though)
    private String radioType;
    private String radioLevel;

    private int majorVersion;
    private int minorVersion;
    private int patchVersion;

    private String trim;
    private String color;

    @Enumerated (EnumType.STRING)
    private VehicleState vehicleState;
    @Enumerated (EnumType.STRING)
    private VehicleUseType vehicleUseType;
    @Enumerated (EnumType.STRING)
    private InfotainmentInterface osType;

    public VehiclesEntity () {

        defaultSettings = new ArrayList<>();
        capabilityEntity = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "VEHICLE ID:" + id + "    VIN:" + vin + "    MMY:" + make + " " +
                model + " " + year;
    }
}

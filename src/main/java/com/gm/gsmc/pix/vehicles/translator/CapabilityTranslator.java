package com.gm.gsmc.pix.vehicles.translator;

import com.gm.gsmc.pix.general.dao.AbstractModelTranslator;
import com.gm.gsmc.pix.model.vehicle.VehicleCapability;
import com.gm.gsmc.pix.vehicles.exception.VehicleExceptionCode;
import com.gm.gsmc.pix.vehicles.jpa.model.CapabilityEntity;
import org.springframework.stereotype.Service;

@Service
public class CapabilityTranslator extends AbstractModelTranslator<CapabilityEntity, VehicleCapability> {
    @Override
    public VehicleCapability translate(CapabilityEntity original) {
        if(original == null){
            throw VehicleExceptionCode.INVALID_CAPABILITIES.exception();
        }
        VehicleCapability capability = new VehicleCapability();
        capability.setName(original.getName());
        capability.setMinValue(original.getMinValue());
        capability.setMaxValue(original.getMaxValue());
        return capability;
    }

}

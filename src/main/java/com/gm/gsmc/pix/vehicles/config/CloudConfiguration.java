package com.gm.gsmc.pix.vehicles.config;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.java.AbstractCloudConfig;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.gm.gsmc.pix.general.cache.CaffeineYamlConfiguration;
import com.gm.gsmc.pix.general.cache.ConfigurableCaffeineCacheManager;
import com.gm.gsmc.pix.vehicles.client.UtilityClient;

@Configuration
@Profile ("cloud")
@EnableDiscoveryClient
@EnableFeignClients (basePackageClasses = UtilityClient.class)
@EnableCircuitBreaker
@EnableCaching
public class CloudConfiguration extends AbstractCloudConfig {

    @Bean
    public CaffeineYamlConfiguration getCaffeineYamlConfiguration() {
        return new CaffeineYamlConfiguration();
    }

    @Bean
    public CacheManager getCacheManager(CaffeineYamlConfiguration ymlConfig) {
        return new ConfigurableCaffeineCacheManager(ymlConfig);
    }
}

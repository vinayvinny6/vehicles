package com.gm.gsmc.pix.vehicles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.cloudfoundry.CloudFoundryConnector;
import org.springframework.context.annotation.ComponentScan;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author TZVZCY
 */
@SpringBootApplication
@EnableSwagger2
@ComponentScan
public class VehiclesApplication {

//    private final MMYRepository mmyRepo;
//    private final VehiclesRepository vehiclesRepo;
//    private final VehicleSettingsRepository vehicleSettingsRepo;
//    private final CapabilityRepository capabilityRepo;
//    private final VehicleVersionRepository vehicleVersionRepo;

    private static final Logger log = LoggerFactory.getLogger(VehiclesApplication.class);

//    @Autowired
//    public VehiclesApplication(VehiclesRepository vehiclesRepo, VehicleSettingsRepository vehicleSettingsRepo, CapabilityRepository capabilityRepo, VehicleVersionRepository vehicleVersionRepo, MMYRepository mmyRepo) {
//        this.vehiclesRepo = vehiclesRepo;
//        this.vehicleSettingsRepo = vehicleSettingsRepo;
//        this.capabilityRepo = capabilityRepo;
//        this.vehicleVersionRepo = vehicleVersionRepo;
//        this.mmyRepo = mmyRepo;
//    }

    public static void main(String[] args) {
        if (new CloudFoundryConnector().isInMatchingCloud()) {
            log.info("Vehicles - Setting profile to CLOUD");
            System.setProperty("spring.profiles.active", "cloud");
        }
        SpringApplication.run(VehiclesApplication.class, args);
    }

//    @Bean
//    public CommandLineRunner demo() {
//        return (args) -> {
//
//        //Vehicle 1
//            VehiclesEntity vehiclesEntity1 = new VehiclesEntity("VIN123",CountryIsoCode.US, VehicleState.Sold, VehicleUseType.Personal);
//            vehiclesRepo.save(vehiclesEntity1);
//            //MMY
//            mmyRepo.save(new MMYEntity("GM","GMC","Sierra", 2016,"LX", vehiclesEntity1));
//            //Vehicle Version
//            vehicleVersionRepo.save(new VehicleVersionEntity(InfotainmentInterface.NGI,1,1,4, vehiclesEntity1));
//            //Vehicle Settings
//            vehicleSettingsRepo.save(new DefaultSettingsEntity(SettingCategory.VehicleSetting, "XMRadio1", "13", SettingType.INT,  vehiclesEntity1));
//            vehicleSettingsRepo.save(new DefaultSettingsEntity(SettingCategory.VehicleSetting, "Radio Level", "20", SettingType.INT, vehiclesEntity1));
//            //Vehicle Capabilities
//            capabilityRepo.save(new CapabilityEntity(vehiclesEntity1, "Window Lock", "1"));
//            capabilityRepo.save(new CapabilityEntity(vehiclesEntity1, "Heated Seat Level", "6"));
//
//        //Vehicle 2
//            VehiclesEntity vehiclesEntity2 = new VehiclesEntity("VIN1234",CountryIsoCode.CA, VehicleState.Demo, VehicleUseType.Business);
//            vehiclesRepo.save(vehiclesEntity2);
//            //MMY
//            mmyRepo.save(new MMYEntity("GM", "Chevrolet","Silverado", 2017,"LT", vehiclesEntity2));
//            //Vehicle Version
//            vehicleVersionRepo.save(new VehicleVersionEntity(InfotainmentInterface.INFO3,14,12,0, vehiclesEntity2));
//            //Capabilities
//            capabilityRepo.save(new CapabilityEntity(vehiclesEntity2, "Door Lockout", "1"));
//            capabilityRepo.save(new CapabilityEntity(vehiclesEntity2, "Radio Maximum", "26"));
//            capabilityRepo.save(new CapabilityEntity(vehiclesEntity2, "Heated Seats", "1"));
//            capabilityRepo.save(new CapabilityEntity(vehiclesEntity2, "Bose Sound System", "yes"));
//
//            // fetch all customers
//            log.info("Vehicles found with findAll():");
//            log.info("-------------------------------");
//            for (VehiclesEntity vehiclesEntity : vehiclesRepo.findAll()) {
//                log.info(vehiclesEntity.toString());
//            }
//            log.info("-------------------------------");
//
//        };
//
//    }
}

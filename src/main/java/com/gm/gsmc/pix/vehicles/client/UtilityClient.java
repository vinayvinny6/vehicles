package com.gm.gsmc.pix.vehicles.client;

import com.gm.gsmc.pix.vehicles.client.fallback.UtilityClientFallback;
import org.springframework.cloud.netflix.feign.FeignClient;
import com.gm.gsmc.pix.api.service.UtilityService;
import org.springframework.context.annotation.Primary;

@Primary
@FeignClient(name = "utility", fallback = UtilityClientFallback.class)
public interface UtilityClient extends UtilityService{

}


package com.gm.gsmc.pix.vehicles.translator;

import com.gm.gsmc.pix.general.dao.AbstractModelTranslator;
import com.gm.gsmc.pix.model.vehicle.MakeModelYear;
import com.gm.gsmc.pix.vehicles.client.UtilityClient;
import com.gm.gsmc.pix.vehicles.exception.VehicleExceptionCode;
import com.gm.gsmc.pix.vehicles.jpa.model.MMYEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author YZ70WZ
 */

@Service
public class MMYTranslator extends AbstractModelTranslator<MMYEntity, MakeModelYear> {

    private final UtilityClient utilityClient;

    @Autowired
    public MMYTranslator(UtilityClient utilityClient){this.utilityClient = utilityClient;}

    @Override
    public MakeModelYear translate(MMYEntity original){
        if(original == null){
            throw VehicleExceptionCode.INVALID_MMY.exception();
        }
        MakeModelYear makeModelYear = new MakeModelYear();
        makeModelYear.setMake(original.getMake());
        makeModelYear.setModel(original.getModel());
        makeModelYear.setYear(original.getYear());

        return makeModelYear;
    }
}

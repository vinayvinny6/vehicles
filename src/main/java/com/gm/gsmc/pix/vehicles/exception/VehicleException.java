package com.gm.gsmc.pix.vehicles.exception;

import com.gm.gsmc.pix.api.service.PixException;

public class VehicleException extends PixException {

    public VehicleException (VehicleExceptionCode code) {
        super(code.getDescription(), code.getCode());
    }

    public VehicleException (VehicleExceptionCode code, Throwable cause) {
        super(code.getDescription(), code.getCode(), cause);
    }
}

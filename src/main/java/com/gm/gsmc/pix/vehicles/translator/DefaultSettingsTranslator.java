package com.gm.gsmc.pix.vehicles.translator;

import com.gm.gsmc.pix.general.dao.AbstractModelTranslator;
import com.gm.gsmc.pix.model.settings.SettingDescriptor;
import com.gm.gsmc.pix.vehicles.exception.VehicleExceptionCode;
import com.gm.gsmc.pix.vehicles.jpa.model.DefaultSettingsEntity;
import org.springframework.stereotype.Service;

@Service
public class DefaultSettingsTranslator extends AbstractModelTranslator<DefaultSettingsEntity, SettingDescriptor> {
    @Override
    public SettingDescriptor translate(DefaultSettingsEntity original){

        if(original == null){
            throw VehicleExceptionCode.INVALID_VEHICLE_SETTINGS.exception();
        }
        SettingDescriptor defaultSettings = new SettingDescriptor();
        defaultSettings.setCategory(original.getCategory());
        defaultSettings.setName(original.getName());
        defaultSettings.setSelectedValue(original.getValue());
        defaultSettings.setType(original.getType());

        return defaultSettings;
    }

}

package com.gm.gsmc.pix.vehicles.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gm.gsmc.pix.api.service.VehiclesService;
import com.gm.gsmc.pix.model.CountryIsoCode;
import com.gm.gsmc.pix.model.settings.SettingDescriptor;
import com.gm.gsmc.pix.model.vehicle.Vehicle;
import com.gm.gsmc.pix.model.vehicle.VehicleCapability;
import com.gm.gsmc.pix.model.vehicle.VehicleState;
import com.gm.gsmc.pix.vehicles.manager.VehiclesManager;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping ("/vehicles")
@Api (description = "Vehicles Service - Manages Vehicle specific data for NGI and Info3")
public class VehiclesController implements VehiclesService {

    @Autowired
    private VehiclesManager vehiclesManager;

    @Override
    @ApiOperation (value = "Creates a new vehicle")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 201, message = "New vehicle object was successfully created!"),
            @ApiResponse (code = 400, message = "The vehicle object created was not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @RequestMapping (value = "/", method = RequestMethod.POST, produces = { "application/json" })
    public Vehicle createVehicle(@RequestBody Vehicle vehicle) {
        return this.vehiclesManager.createVehicle(vehicle);
    }

    @Override
    @ApiOperation (value = "Gets a vehicle object through VIN")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The VIN # used was not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @RequestMapping (value = "/{vin}", method = RequestMethod.GET, produces = { "application/json" })
    public Vehicle getVehicle(
            @PathVariable ("vin") @ApiParam (value = "The vin of the vehicle used to get Vehicle info") String vin) {
        return this.vehiclesManager.getVehicle(vin);
    }

    @Override
    @ApiOperation (value = "Grabs a vehicle's Country ISO Code through VIN")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The VIN # used was not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @RequestMapping (value = "/{vin}/country", method = RequestMethod.GET, produces = { "application/json" })
    public CountryIsoCode getVehicleCountry(
            @PathVariable ("vin") @ApiParam (value = "The vin of the vehicle that will be updated") String vin) {
        return this.vehiclesManager.getVehicleCountry(vin);
    }

    @Override
    @ApiOperation (value = "Updates vehicle country through VIN")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The VIN # or Country Name is not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @RequestMapping (value = "/{vin}/{isoCode}", method = RequestMethod.PUT, produces = { "application/json" })
    public CountryIsoCode updateVehicleCountry(
            @PathVariable ("vin") @ApiParam (value = "The vin of the vehicle update vehicle information for") String vin,
            @PathVariable ("isoCode") @ApiParam (value = "The ISO code that will be sent for UPDATE") CountryIsoCode isoCode) {
        return this.vehiclesManager.updateVehicleCountry(vin, isoCode);
    }

    @Override
    @ApiOperation (value = "Gets a vehicle's list of capabilities through a specified VIN")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The VIN # used was not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @RequestMapping (value = "/{vin}/capabilities", method = RequestMethod.GET, produces = { "application/json" })
    public List<VehicleCapability> getVehicleCapabilities(
            @PathVariable ("vin") @ApiParam (value = "The vin of the vehicle used to get the capabilities for") String vin) {
        return this.vehiclesManager.getVehicleCapabilities(vin);
    }

    @Override
    @ApiOperation (value = "Gets a vehicle's list of default settings through a specified VIN")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The VIN # used was not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @RequestMapping (value = "/{vin}/defaultSettings", method = RequestMethod.GET, produces = { "application/json" })
    public List<SettingDescriptor> getDefaultVehicleSettings(
            @PathVariable ("vin") @ApiParam (value = "The vin of the vehicle used to get the capabilities for") String vin) {
        return this.vehiclesManager.getDefaultVehicleSettings(vin);
    }

    @Override
    @ApiOperation (value = "Sets a vehicle's new capabilities and overwrites the old ones")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The VIN # used was not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @RequestMapping (value = "/{vin}/capabilities", method = RequestMethod.POST)
    public List<VehicleCapability> setVehicleCapabilities(
            @PathVariable ("vin") @ApiParam (value = "The vin of the vehicle used to get the capabilities for") String vin,
            @RequestBody @ApiParam (value = "The list of capabilities that will be grabbed", required = true) List<VehicleCapability> vehicleCapabilities) {

        return vehiclesManager.setVehicleCapabilities(vin, vehicleCapabilities);
    }

    @Override
    @ApiOperation (value = "Updates a vehicle's state to: DEMO, SOLD, or TRIAL")
    @ApiResponses (value = {
            @ApiResponse (code = 200, message = "Successful response"),
            @ApiResponse (code = 400, message = "The VIN # used was not valid"),
            @ApiResponse (code = 500, message = "Internal system error") })
    @RequestMapping (value = "/{vin}", method = RequestMethod.PUT, produces = { "application/json" })
    public Vehicle updateVehicleState(
            @PathVariable ("vin") @ApiParam ("VIN used to update Infotainment state") String vin,
            @RequestParam ("vehicleState") @ApiParam (value = "Flag that denotes if vehicle is in state: DEMO or SOLD") VehicleState vehicleState) {
        return this.vehiclesManager.updateInfotainmentState(vin, vehicleState);
    }
}

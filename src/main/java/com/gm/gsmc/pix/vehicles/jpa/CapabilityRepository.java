package com.gm.gsmc.pix.vehicles.jpa;

import com.gm.gsmc.pix.vehicles.jpa.model.CapabilityEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public interface CapabilityRepository extends CrudRepository<CapabilityEntity, Long> {

        CapabilityEntity findOneByName(String name);

}


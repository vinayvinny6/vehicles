package com.gm.gsmc.pix.vehicles.jpa;

import com.gm.gsmc.pix.vehicles.jpa.model.MMYEntity;
import org.springframework.data.repository.CrudRepository;

public interface MMYRepository extends CrudRepository<MMYEntity, Long> {
    //find a MMY by generated ID
    MMYEntity findOneById(Long id);
}

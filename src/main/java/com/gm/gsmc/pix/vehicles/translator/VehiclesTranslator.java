package com.gm.gsmc.pix.vehicles.translator;

import com.gm.gsmc.pix.general.dao.AbstractModelTranslator;
import com.gm.gsmc.pix.model.vehicle.Vehicle;
import com.gm.gsmc.pix.vehicles.exception.VehicleExceptionCode;
import com.gm.gsmc.pix.vehicles.jpa.model.VehiclesEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VehiclesTranslator extends AbstractModelTranslator<VehiclesEntity, Vehicle>{


    private final CapabilityTranslator capabilityTranslator;
    private final DefaultSettingsTranslator defaultSettingsTranslator;

    @Autowired
    public VehiclesTranslator(CapabilityTranslator capabilityTranslator, DefaultSettingsTranslator defaultSettingsTranslator) {
        this.capabilityTranslator = capabilityTranslator;
        this.defaultSettingsTranslator = defaultSettingsTranslator;
    }

    @Override
    public Vehicle translate(VehiclesEntity original) {
        if(original == null){
            throw VehicleExceptionCode.INVALID_INPUT.exception();
        }
        Vehicle vehicle = new Vehicle();
        vehicle.setVin(original.getVin());
        vehicle.setYear(original.getYear());
        vehicle.setMake(original.getMake());
        vehicle.setModel(original.getModel());
        vehicle.setRadioType(original.getRadioType());
        vehicle.setRadioLevel(original.getRadioLevel());
        vehicle.setTrim(original.getTrim());
        vehicle.setColor(original.getColor());
        vehicle.setMinorVersion(original.getMinorVersion());
        vehicle.setMajorVersion(original.getMajorVersion());
        vehicle.setPatchVersion(original.getPatchVersion());

        if(original.getIsoCode() != null) {
            vehicle.setIsoCode(original.getIsoCode());
        }
        if(original.getCapabilityEntity() != null) {
            vehicle.setCapabilities(capabilityTranslator.translate(original.getCapabilityEntity()));
        }
        if(original.getDefaultSettings() != null){
            vehicle.setDefaultSettings(defaultSettingsTranslator.translate(original.getDefaultSettings()));
        }
        if(original.getVehicleState() != null){
            vehicle.setVehicleState(original.getVehicleState());
        }
        if(original.getVehicleUseType() != null){
            vehicle.setVehicleUseType(original.getVehicleUseType());
        }
        if(original.getOsType() != null){
            vehicle.setOsType(original.getOsType());
        }

        return vehicle;
    }

}
